package de.luedtke;

import de.luedtke.ssp.Gewinner;
import de.luedtke.ssp.Hand;
import de.luedtke.ssp.Richter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for Schere Stein Papier App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Richter Test
     */
    public void testRichter()
    {
        // Schere schlägt Papier
        assertEquals(Richter.werIstGewinner(Hand.SCHERE, Hand.PAPIER), Gewinner.SPIELER);
        assertEquals(Richter.werIstGewinner(Hand.PAPIER, Hand.SCHERE), Gewinner.COMPUTER);

        // Papier schlägt Stein
        assertEquals(Richter.werIstGewinner(Hand.PAPIER, Hand.STEIN), Gewinner.SPIELER);
        assertEquals(Richter.werIstGewinner(Hand.STEIN, Hand.PAPIER), Gewinner.COMPUTER);

        // Stein schlägt Schere
        assertEquals(Richter.werIstGewinner(Hand.STEIN, Hand.SCHERE), Gewinner.SPIELER);
        assertEquals(Richter.werIstGewinner(Hand.SCHERE, Hand.STEIN), Gewinner.COMPUTER);

        // Unentschieden
        assertEquals(Richter.werIstGewinner(Hand.SCHERE, Hand.SCHERE), Gewinner.UNENTSCHIEDEN);
        assertEquals(Richter.werIstGewinner(Hand.STEIN, Hand.STEIN), Gewinner.UNENTSCHIEDEN);
        assertEquals(Richter.werIstGewinner(Hand.PAPIER, Hand.PAPIER), Gewinner.UNENTSCHIEDEN);
    }
}
