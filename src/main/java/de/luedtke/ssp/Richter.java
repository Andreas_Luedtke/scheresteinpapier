package de.luedtke.ssp;

public class Richter {

    public static Gewinner werIstGewinner(Hand spieler, Hand computer) {
        if (spieler == computer) {
            return Gewinner.UNENTSCHIEDEN;
        } else {
            if ((spieler == Hand.SCHERE && computer == Hand.PAPIER) ||
                    (spieler == Hand.PAPIER && computer == Hand.STEIN) ||
                    (spieler == Hand.STEIN && computer == Hand.SCHERE)) {
                return Gewinner.SPIELER;
            }
            return Gewinner.COMPUTER;
        }
    }
}
