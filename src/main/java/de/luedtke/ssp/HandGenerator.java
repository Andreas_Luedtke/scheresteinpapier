package de.luedtke.ssp;

import java.util.concurrent.ThreadLocalRandom;

public class HandGenerator {
    public static Hand getRandomHand() {
        return Hand.getHand(ThreadLocalRandom.current().nextInt(0, 3));
    }
}
