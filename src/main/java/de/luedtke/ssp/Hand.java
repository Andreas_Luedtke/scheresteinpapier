package de.luedtke.ssp;

public enum Hand {
    SCHERE,
    STEIN,
    PAPIER;

    public static Hand getHand(int wert) {
        if (wert == SCHERE.ordinal()) {
            return SCHERE;
        } else if (wert == STEIN.ordinal()) {
            return STEIN;
        } else if (wert == PAPIER.ordinal()) {
            return PAPIER;
        }
        return null;
    }
}
