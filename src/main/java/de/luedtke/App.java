package de.luedtke;

import de.luedtke.ssp.Gewinner;
import de.luedtke.ssp.Hand;
import de.luedtke.ssp.HandGenerator;
import de.luedtke.ssp.Richter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    private static final int UNGUELTIG = -1;

    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Hand computerHand = HandGenerator.getRandomHand();
        System.out.println("Schere, Stein, Papier");
        System.out.println("---------------------");
        Hand spielerHand;
        int wahl;
        do {
            System.out.println("Bitte wählen Sie 0 für Schere, 1 für Stein oder 2 für Papier");
            System.out.println("Ihre Eingabe? (Mit Enter abschließen) ");
            try {
                String eingabe = br.readLine();
                wahl = Integer.parseInt(eingabe);
            } catch (IOException | NumberFormatException ex) {
                wahl = UNGUELTIG;
            }
            spielerHand = Hand.getHand(wahl);
            if (spielerHand == null) {
                System.out.println("Ungültige Eingabe, bitte geben Sie eine Zahl zwischen 0 und 2 ein");
            } else {
                System.out.println("Computer wählt: " + computerHand);
                System.out.println("Spieler wählt:  " + spielerHand);
                Gewinner gewinner = Richter.werIstGewinner(spielerHand, computerHand);
                if (gewinner == Gewinner.UNENTSCHIEDEN) {
                    System.out.println("Unentschieden!");
                } else {
                    System.out.println((gewinner == Gewinner.SPIELER) ? (spielerHand + " schlägt " + computerHand) : (computerHand + " schlägt " + spielerHand));
                    System.out.println("Spieler hat " + ((gewinner == Gewinner.SPIELER) ? "gewonnen!" : "verloren"));
                }
            }
        } while (spielerHand == null);
    }
}
